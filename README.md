<br>
<div align="center">
    <h1>Gitlab - Scraper<h1>
    <h2>Analyze your Gitlab projects with ease</h2>
</div>

👷 It is currently under development and is not yet ready for use.

<details>
    <summary>Table of Contents</summary>
</details>

## About Gitlab - Scraper

It is a tool made to analyze a Gitlab subdomain and extract information about the projects to find potential vulnerabilities such as exposed credentials, exposed API keys, etc.

### Built With

- [Node.js](https://nodejs.org/en/)
- [Node-Gitlab](https://www.npmjs.com/package/node-gitlab)
- [Express](https://expressjs.com/) (soon)

### Features

Current features:

- [x] Project listing
  - [x] Project information
  - [x] Store
- [ ] Project Analysis
- [ ] Vulnerability Detection

## Getting Started

### Prerequisites

- [Node.js](https://nodejs.org/en/)

### Installation

1. Clone the repo
   ```sh
   git clone https://gitlab.isima.fr/rovandemer/gitlabscanner
   ```

2. Install NPM packages
   ```sh
    npm install
    ```

3. Create the `src/data` folder, and add a `config.json` file with the following body :

```json
{
    "api": "https://gitlab.domain.fr/api/v4",
    "privateToken": "WonderfulPrivateToken",
}
```

4. Run the script
   ```sh
   npm run test
   ```