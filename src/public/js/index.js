// Ultra low performance code, but it works 🫣
let projects = {};

// When window loads, run the function
$(window).on("load", function () {
    // When "#load-projects" is clicked, run the function
    $("#load-projects").on("click", function () {
        // Fetch projects "/projects"
        fetch("/projects")
            .then((response) => {
                return response.json();
            })
            .then((data) => {
                // Concatenate the projects
                projects = jsonConcat(projects, data);

                if (!data || data.length == 0) {
                    alert("No projects found.");
                    return;
                }

                // Display the projects
                display();
            });
    });
});

// When "#search" input changes, run the function
$("#search").on("input", function () {
    // Display the projects
    display();
});

// When "#tested" input changes, run the function
$("#tested").on("change", function () {
    // Display the projects
    display();
});

// When "#vulnerable" input changes, run the function
$("#vulnerable").on("change", function () {
    // Display the projects
    display();
});

function display() {
    if (!projects || projects.length == 0) {
        alert("No projects found.");
        return;
    }

    // Clear the project container "#project-container"
    clearProjectContainer();

    // Filter the projects
    let filteredProjects = filterProjects();

    // Add each project to the project container "#project-container"
    for (let project in filteredProjects) {
        addProjectToContainer(
            filteredProjects[project].data,
            project.checked,
            false
        ); // TODO: Add tested and vulnerable
    }
}

// Clear the project container "#project-container"
function clearProjectContainer() {
    $("#projects").html("");
}

// Add a project to the project container "#projects"
function addProjectToContainer(project, tested, vulnerable) {
    // Cannot add image because it has to be fetched with the API
    $("#projects").append(`
        <tr class="project${vulnerable ? ` vulnerable` : ``}">
            <td>${project.name}</td>
            <td>${project.description}</td>
            <td>${project.visibility}</td>
            <td><a href="${project.namespace.web_url}">${
        project.namespace.path
    }<a></td>
            <td><a href="${project.web_url}" target="_blank">🔭</a></td>
            ${
                tested
                    ? `<td><a href="/test/${project.id}">✅</a></td>`
                    : `<td><a href="/test/${project.id}">❌</a></td>`
            }
        </tr>
    `);
}

function jsonConcat(o1, o2) {
    for (var key in o2) {
        o1[key] = o2[key];
    }
    return o1;
}

function filterProjects() {
    // Get the input text "#search"
    let searchBarValue = $("#search").val() || "";
    let testedInput = $("#tested").is(":checked");
    let vulnerableInput = $("#vulnerable").is(":checked");

    if (searchBarValue.length == 0) return projects;

    // Filter the projects
    let filteredProjects = Object.values(projects).filter((project) => {
        // Tested input
        if (testedInput) {
            if (!project.checked == true) {
                return false;
            }
        }

        // Vulnerable input
        // if (vulnerableInput) {
        //     // todo
        // }

        // Search bar
        if (
            project.data.name.toLowerCase().includes(searchBarValue.toLowerCase()) ||
            project.data.description.toLowerCase().includes(searchBarValue.toLowerCase()) ||
            project.data.namespace.path.toLowerCase().includes(searchBarValue.toLowerCase())
        ) {
            return true;
        }

        return false;
    });

    return filteredProjects;
}
