/**
 * @fileoverview This file contains the InvalidProject class. This class is used to throw an error when a project is invalid.
 * @package util
 * @module util/InvalidProject
 * @requires module:util/Logger
 * @version 0.0.1
 * @since 0.0.1
 * @exports InvalidProject
 * @class InvalidProject
 * Represents an invalid project.
 */
class InvalidProject extends Error {

    /**
     * Creates an instance of InvalidProject.
     * @param {string} message The message of the error.
     * @memberof InvalidProject
     * @constructor
     */
    constructor(message) {
        super(message);
        this.name = "InvalidProject";
    }
}

module.exports = InvalidProject;