// Imports
const path = require("path");
const gitlabConnection = require(path.join(__dirname, "util", "Connection.js"));
const Storeable = require(path.join(__dirname, "util", "Storeable.js"));
const express = require("express");
const ejs = require("ejs");

// Config
const config = require(path.join(__dirname, "data", "config.json"));
let store = new Storeable(path.join(__dirname, "data", "projects.json"));

// ------------------------------ Gitlab ------------------------------ //

// Create connection
let connection = new gitlabConnection(config.api, config.privateToken);

// Fetch projects
connection.updateProjects({}, store);
// connection.fetchAllProjects({}, store);

// ------------------------------ Express ------------------------------ //

// Create express app
const app = express();

// Set view engine
app.set("view engine", "ejs");

// Set static folder
app.use(express.static(path.join(__dirname, "public")));

// Views
app.set("views", path.join(__dirname, "views"));

// Routes
app.get("/", (req, res) => {
    res.render("index", {
        projects: store.getData(),
    });
});

app.get("/projects", (req, res) => {
    // Return json
    res.json(store.getData());
});

// Start server
app.listen(8081, () => {
    console.log(`✅  -- Server started on port 8081`);
});