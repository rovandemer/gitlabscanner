// Imports
const invalidProject = require("./InvalidProject.js");
const Storeable = require("./Storeable.js");

/**
 * @fileoverview This file contains the Project class.
 * @package util
 * @module util/Project
 * @requires module:util/Logger
 * @requires module:util/Storeable
 * @version 0.0.1
 * @since 0.0.1
 * @exports Project
 * @class Project
 * Represents a project from gitlab.
 */
class Project {
    /**
     * @param {Object} data The data of the project.
     * @param {string} data.id The id of the project.
     * @param {string} data.name The name of the project.
     * @param {string} data.description The description of the project.
     * @param {Object} data.namespace The namespace of the project.
     * @param {string} data.web_url The web url of the project.
     * @param {string} data.avatar_url The avatar url of the project.
     * @param {string} data.ssh_url_to_repo The git ssh url of the project.
     * @param {string} data.http_url_to_repo The git http url of the project.
     * @param {string} data.visibility The visibility level of the project.
     * @param {string} data.last_activity_at The last activity date of the project.
     * @param {boolean} data.empty_repo Whether the project is empty or not.
     * @memberof Project
     * @constructor
     */
    constructor(data) {

        if (!data)
            throw new invalidProject("Invalid project.", "No data provided.");

        // If id, name, or any of the git links is not defined, throw an error.
        if (
            !data.id ||
            !data.name ||
            !data.web_url ||
            !data.ssh_url_to_repo ||
            !data.http_url_to_repo
        ) {
            throw new invalidProject("Invalid project.", "Missing data.");
        }

        /**
         * The id of the project.
         * @type {string}
         * @public
         * @memberof Project
         */
        this.id = data.id;

        /**
         * The name of the project.
         * @type {string}
         * @public
         * @memberof Project
         */
        this.name = data.name;

        /**
         * The description of the project.
         * @type {string}
         * @public
         * @memberof Project
         */
        this.description = data.description || "No description provided.";

        /**
         * The web url of the project.
         * @type {string}
         * @public
         * @memberof Project
         */
        this.web_url = data.web_url;

        /**
         * The avatar url of the project.
         * @type {string}
         * @public
         * @memberof Project
         */
        this.avatar_url =
            data.avatar_url ||
            "https://www.radiologie-barr.fr/wp-content/uploads/scanner-benfeld-1.jpeg";

        /**
         * The git ssh url of the project.
         * @type {string}
         * @public
         * @memberof Project
         */
        this.ssh_url_to_repo = data.ssh_url_to_repo;

        /**
         * The git http url of the project.
         * @type {string}
         * @public
         * @memberof Project
         */
        this.http_url_to_repo = data.http_url_to_repo;

        /**
         * The visibility level of the project.
         * @type {string}
         * @public
         * @memberof Project
         */
        this.visibility = data.visibility;

        /**
         * The namespace of the project.
         * @type {Object}
         * @public
         * @memberof Project
         * @property {string} id The id of the namespace.
         * @property {string} name The name of the namespace.
         * @property {string} path The path of the namespace.
         * @property {string} kind The kind of the namespace.
         * @property {string} full_path The full path of the namespace.
         * @property {string} parent_id The parent id of the namespace.
         * @property {string} avatar_url The avatar url of the namespace.
         * @property {string} web_url The web url of the namespace.
         */
        this.namespace = data.namespace;

        /**
         * The last activity date of the project.
         * @type {Date}
         * @public
         * @memberof Project
         */
        this.last_activity_at = new Date(data.last_activity_at); // Helps to check a project once.

        /**
         * Whether the project is empty or not.
         * @type {boolean}
         * @public
         * @memberof Project
         * @readonly
         * @default false
         */
        this.empty_repo = data.empty_repo || false;
    }

    /**
     * Creates a Project from data.
     * @param {Object} data The data of the project.
     * @returns {Project} The project created from the data.
     */
    static fromData(data) {
        return new Project(data);
    }

    /**
     * Saves the project to the file.
     * @memberof Project
     * @public
     * @param {Storeable} storeable The storeable to save the project to.
     * @returns {boolean} Whether the project was saved or not.
     * @see {@link Storeable#save}
     */
    save(storeable) {

        // Save the project.
        return storeable.store({
            id: this.id,
            name: this.name,
            description: this.description,
            web_url: this.web_url,
            avatar_url: this.avatar_url,
            ssh_url_to_repo: this.ssh_url_to_repo,
            http_url_to_repo: this.http_url_to_repo,
            visibility: this.visibility,
            namespace: this.namespace,
            last_activity_at: this.last_activity_at,
            empty_repo: this.empty_repo,
        }, this.last_activity_at.getTime());

    }
    
}

module.exports = Project;
