// Import
const fs = require("fs");


/**
 * @fileoverview This file contains the Storeable class. This class is used to store data in a file.
 * @package util
 * @module util/Storeable
 * @requires module:util/Logger
 * @version 0.0.1
 * @since 0.0.1
 * @exports Storeable
 * @class Storeable
 * Represents a storeable object.
 */
class Storeable {

    /**
     * @param {string} path The path to the file to store the data in (json).
     * @memberof Storeable
     * @constructor
     */
    constructor(path) {

        /**
         * The path to the file to store the data in (json).
         * @type {string}
         * @private
         * @memberof Storeable
         */
        this._path = path;

        // Create the file if it doesn't exist.
        this.createFile(this._path).then(() => {
            /**
             * The data from the file.
             * @type {Object}
             * @private
             * @memberof Storeable
             */
            this._data = require(this._path);

            /**
             * The size of the data.
             * @type {number}
             * @public
             * @memberof Storeable
             * @readonly
             */
            this.size = this._data.length;
        });
    }

    /**
     * Create the file if it doesn't exist.
     * @memberof Storeable
     * @public
     * @returns {Promise} A promise that resolves when the file is created.
     * @static
     */
    createFile(path) {

        // Check if the file exists.
        return new Promise((resolve, reject) => {
            fs.access(path, fs.constants.F_OK, (err) => {

                // If there is an error, reject the promise.    
                if (err)
                    reject(err);
                
                // Resolve the promise.
                resolve();
            });
        }).catch(() => {

            // Return a promise.
            return new Promise((resolve, reject) => {

                // Create the file.
                fs.writeFile(path, "{}", (err) => {

                    // If there is an error, reject the promise.
                    if (err)
                        reject(err);
                    
                    // Resolve the promise.
                    resolve();
                });
            });
        });
    }

    /**
     * Saves the data to the file.
     * @memberof Storeable
     * @public
     * @param {Object} data The data to change.
     * @returns {boolean} Whether the data was saved.
     */
    store(data, date) {

        // If no data is provided, throw an error.
        if (!data)
            throw new Error("No data provided.");

        // If the data can't be saved, return false.
        if (!this.canStore(data, date))
            return false;
        
        // Merge the data.
        this._data[data.id] = {
            data: data,
            date: date,
            checked: false,
            vulnerable: false,
        };

        fs.writeFileSync(this._path, JSON.stringify(this._data, null, 4));

        this.size += 1;

        return true;

    }

    /**
     * Can the data be saved? -> Check if the data is already saved and if it is, check if the date is newer.
     * @memberof Storeable
     * @public
     * @param {Object} data The data to change.
     * @returns {boolean} Whether the data can be saved.
     */
    canStore(data, date) {

        // If no data is provided, throw an error.
        if (!data)
            throw new Error("No data provided.");
        
        // If the data is not saved, return true.
        if (!this._data[data.id])
            return true;
        
        // If the date is newer, return true.
        if (this._data[data.id].date < date)
            return true;
        
        // Else, return false.
        return false;

    }


    /**
     * Gets the data from the file.
     * @memberof Storeable
     * @public
     * @returns {Object} The data from the file.
     */
    get() {
        return this._data;
    }

    /**
     * Clears the data from the file.
     * @memberof Storeable
     * @public
     * @returns {Promise} A promise that resolves when the data is cleared.
     */
    clear() {

        // Return a promise.
        return new Promise((resolve, reject) => {

            // Write the data to the file (add indentation)
            fs.writeFile(this._path, "{}", (err) => {

                // If there is an error, reject the promise.
                if (err)
                    reject(err);

                // Resolve the promise and update the data.
                this._data = require(this._path);
                resolve();
            });
        });
    }

    /**
     * Returns the data as a JSON.
     * @memberof Storeable
     * @public
     * @returns {Object} The data as an array.
     * @readonly
     */
    getData() {
        return this._data;
    }

}

module.exports = Storeable;
