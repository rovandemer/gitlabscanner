// Imports
const gitlabAPI = require("node-gitlab");
const Project = require("./Project.js");

/**
 * @fileoverview This file contains the Connection class.
 * @package util
 * @module util/Connection
 * @requires module:util/Logger
 * @requires module:data/config.json
 * @requires module:node-gitlab
 * @version 0.0.1
 * @since 0.0.1
 * @exports Connection
 * @class Connection
 * Represents a connection with gitlab's API
 */
class Connection {
    /**
     * Creates an instance of Connection.
     * @param {string} [api] The API to use to connect to the API.
     * @param {string} [privateToken] The private token to use to connect to the API.
     * @memberof Connection
     * @constructor
     */
    constructor(api, privateToken) {
        /**
         * The private token to use to connect to the API.
         * @type {string}
         * @private
         * @memberof Connection
         */
        this._privateToken =
            privateToken || require("../data/config.json").privateToken;

        /**
         * The API to use to connect to the API.
         * @type {string}
         * @private
         * @memberof Connection
         */
        this._api = api || require("../data/config.json").api;

        /**
         * The client to use to connect to the API.
         * @type {Object}
         * @private
         * @memberof Connection
         */
        this._client = gitlabAPI.create({
            api: this._api,
            privateToken: this._privateToken,
        });
    }

    /**
     * Fetches projects from the API.
     * @param {Object} [options] The options to use to fetch the projects.
     * @param {number} [options.per_page] The number of projects to fetch per page.
     * @param {number} [options.page] The page to fetch.
     * @param {string} [options.search] The search to use to fetch the projects.
     * @param {string} [options.owned] Whether to fetch the projects owned by the user.
     * @memberof Connection
     */
    fetchProjects(options) {
        return new Promise((resolve, reject) => {
            this._client.projects.list(options, (err, projects) => {
                if (err) {
                    reject(err);
                } else {
                    resolve(
                        projects.map((project) => {
                            return Project.fromData(project);
                        })
                    );
                }
            });
        });
    }

    /**
     * Fetches all projects from the API.
     * @param {Object} [options] The options to use to fetch the projects.
     * @param {number} [options.owned] Whether to fetch the projects owned by the user.
     * @param {number} [options.per_page] The number of projects to fetch per page.
     * @param {number} [options.start_page] The page to start fetching from.
     * @param {number} [options.end_page] The page to end fetching at.
     * @param {boolean} [options.display] Whether to display the projects.
     * @param {Storeable} [store] The store to use to store the projects.
     * @returns {Promise} A promise that resolves when all projects are fetched.
     * @memberof Connection
     */
    fetchAllProjects(options, store) {
        if (!store) throw new Error("No store provided.");

        options = options || {};

        options.per_page = options.per_page || 100;
        options.current_page = options.start_page || 1;
        options.end_page = options.end_page || 100;
        options.display = options.display || true;

        return new Promise((resolve, reject) => {
            this.recursiveFetchAllProjects(options, store);

            resolve();
        });
    }

    /**
     * Recursively fetches all projects from the API.
     * @param {Object} options The options to use to fetch the projects.
     * @param {number} options.per_page The number of projects to fetch per page.
     * @param {number} options.current_page The page to fetch.
     * @param {number} options.end_page The page to end fetching at.
     * @param {boolean} options.display Whether to display the projects.
     * @param {Storeable} store The store to use to store the projects.
     * @memberof Connection
     * @private
     * @returns {void}
     */
    recursiveFetchAllProjects(options, store, tries_left=3) {
        console.log(`Fetching page ${options.current_page}...`);

        if (options.current_page > options.end_page) return;

        this.fetchProjects({
            per_page: options.per_page,
            page: options.current_page,
        })
            .then((projects) => {
                if (projects.length !== 0) {
                    projects.forEach((project) => {
                        let hasSaved = project.save(store);
                        if (options.display) {
                            if (hasSaved) {
                                console.log(
                                    "✅ Saved project " + project.name + "."
                                );
                            }
                        }
                    });

                    // Timeout to prevent rate limiting.
                    setTimeout(() => {
                        this.recursiveFetchAllProjects(
                            {
                                per_page: options.per_page,
                                current_page: options.current_page + 1,
                                end_page: options.end_page,
                                display: options.display,
                            },
                            store
                        );
                    }, 5000);
                }
            })
            .catch((err) => {
                if (tries_left > 0) {
                    console.log("🛑 Error fetching projects, retrying...");
                    setTimeout(() => {
                        this.recursiveFetchAllProjects(
                            {
                                per_page: options.per_page,
                                current_page: options.current_page,
                                end_page: options.end_page,
                                display: options.display,
                            },
                            store,
                            tries_left - 1
                        );
                    }, 10000 + Math.random() * 10000);
                } else {
                    throw err;
                }
            });
    }

    /**
     * Updates projects : search until a non-existing / non-updated project is found.
     * @param {Object} [options] The options to use to fetch the projects.
     * @param {number} [options.per_page] The number of projects to fetch per page.
     * @param {number} [options.page] The page to fetch.
     * @param {string} [options.search] The search to use to fetch the projects.
     * @param {string} [options.owned] Whether to fetch the projects owned by the user.
     * @param {Storeable} [store] The store to use to store the projects.
     * @memberof Connection
     * @returns {Promise} A promise that resolves when all projects are updated.
     */
    updateProjects(options, store) {

        options = options || {};

        options.per_page = options.per_page || 100;
        options.current_page = options.start_page || 1;
        options.end_page = options.end_page || 100;
        options.display = options.display || true;

        return new Promise((resolve, reject) => {
            this.recursiveUpdateProjects(options, store);

            resolve();
        });

    }

    /**
     * Recursively updates projects : search until a non-existing / non-updated project is found.
     * @param {Object} options The options to use to fetch the projects.
     * @param {number} options.per_page The number of projects to fetch per page.
     * @param {number} options.current_page The page to fetch.
     * @param {number} options.end_page The page to end fetching at.
     * @param {boolean} options.display Whether to display the projects.
     * @param {Storeable} store The store to use to store the projects.
     * @memberof Connection
     * @private
     * @returns {void}
     */
    recursiveUpdateProjects(options, store, tries_left=3) {
        console.log(`Updating page ${options.current_page}...`);

        if (options.current_page > options.end_page) return;

        this.fetchProjects({
            per_page: options.per_page,
            page: options.current_page,
        })
            .then((projects) => {
                if (projects.length !== 0) {

                    let hasSaved = true;

                    projects.forEach((project) => {
                        let hasUpdated = project.save(store);
                        if (options.display) {
                            if (hasUpdated) {
                                console.log(
                                    "✅ Updated project " + project.name + "."
                                );
                            }
                        }

                        hasSaved = hasSaved && hasUpdated;
                    });

                    if (!hasSaved) {
                        console.log("🛑 No more projects to update.");
                        return;
                    } else {

                        // Timeout to prevent rate limiting.
                        setTimeout(() => {
                            this.recursiveUpdateProjects(
                                {
                                    per_page: options.per_page,
                                    current_page: options.current_page + 1,
                                    end_page: options.end_page,
                                    display: options.display,
                                },
                                store
                            );
                        }, 5000);
                        
                    }
                }
            })
            .catch((err) => {
                if (tries_left > 0) {
                    console.log("🛑 Error updating projects, retrying...");
                    setTimeout(() => {
                        this.recursiveUpdateProjects(
                            {
                                per_page: options.per_page,
                                current_page: options.current_page,
                                end_page: options.end_page,
                                display: options.display,
                            },
                            store,
                            tries_left - 1
                        );
                    }, 10000 + Math.random() * 10000);
                } else {
                    throw err;
                }
            });
    }
}

module.exports = Connection;
